import React, { FC, SVGProps } from 'react';

const WalletConnectIcon: FC<SVGProps<SVGElement>> = ({ className }) => (
  <svg viewBox="0 0 40 40" className={className}>
    <path
      fill="url(#paint0_radial_1762_4955)"
      d="M40 20C40 8.954 31.046 0 20 0S0 8.954 0 20s8.954 20 20 20 20-8.954 20-20z"
    ></path>
    <path
      fill="#fff"
      d="M13.22 14.417c3.744-3.667 9.816-3.667 13.56 0l.451.441a.462.462 0 010 .664l-1.542 1.51a.243.243 0 01-.338 0l-.62-.608c-2.613-2.558-6.849-2.558-9.461 0l-.664.65a.243.243 0 01-.34 0l-1.541-1.509a.462.462 0 010-.664l.495-.484zm16.749 3.122l1.372 1.343a.463.463 0 010 .664l-6.187 6.058a.487.487 0 01-.678 0l-4.391-4.3a.122.122 0 00-.17 0l-4.39 4.3a.487.487 0 01-.679 0L8.66 19.546a.463.463 0 010-.664l1.372-1.344a.487.487 0 01.678 0l4.391 4.3a.122.122 0 00.17 0l4.39-4.3a.487.487 0 01.679 0l4.391 4.3a.122.122 0 00.17 0l4.39-4.3a.487.487 0 01.679 0z"
    ></path>
    <defs>
      <radialGradient
        id="paint0_radial_1762_4955"
        cx="0"
        cy="0"
        r="1"
        gradientTransform="matrix(40 0 0 40 0 20)"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#5D9DF6"></stop>
        <stop offset="1" stopColor="#006FFF"></stop>
      </radialGradient>
    </defs>
  </svg>
);

export default WalletConnectIcon;
