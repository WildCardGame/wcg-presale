import React, { FC, SVGProps } from 'react';

const MetamaskIcon: FC<SVGProps<SVGElement>> = ({ className }) => (
  <svg viewBox="0 0 40 40" className={className}>
    <path
      fill="#E4761B"
      stroke="#E4761B"
      strokeWidth="0.19"
      d="M35.017 26.418l-2.02 6.57-3.967-1.044 5.987-5.526z"
    ></path>
    <path
      fill="#E4761B"
      stroke="#E4761B"
      strokeWidth="0.19"
      d="M29.03 31.944l3.826-5.04 2.161-.486-5.987 5.526z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M32.044 21.465l2.973 4.953-2.162.485-.811-5.438zm0 0l1.913-1.275 1.06 6.227-2.973-4.953z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M29.61 17.723l5.498-2.166-.273 1.186-5.226.98zm5.05.446l-5.05-.446 5.225-.98-.174 1.426z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M34.66 18.169l-.703 2.022-4.348-2.468 5.052.446zm1.102-2.11l-.927.684.273-1.186.654.501zm-1.101 2.11l.174-1.426.737.59-.911.836z"
    ></path>
    <path
      fill="#E2761B"
      stroke="#E2761B"
      strokeWidth="0.19"
      d="M24.765 30.344l1.375.422 2.89 1.178-4.265-1.6z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M33.957 20.191l.704-2.022.605.445-1.309 1.577zm0 0l-5.706-1.807 1.359-.661 4.347 2.468zM30.968 11.519l-1.358 6.203-1.358.66 2.716-6.863z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M35.108 15.556l-5.499 2.166 1.358-6.203 4.141 4.037zM30.968 11.519l5.738-.637-1.598 4.674-4.14-4.037z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M33.957 20.19l-1.913 1.274-3.792-3.081 5.705 1.807z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M36.458 6.415l.248 4.467-5.738.637 5.49-5.104z"
    ></path>
    <path
      fill="#E2761B"
      stroke="#E2761B"
      strokeWidth="0.19"
      d="M36.458 6.415l-11.47 8.019-.148-5.439 11.618-2.58z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M16.31 8.342l8.53.653.15 5.439-8.68-6.092z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M28.252 18.383l-3.263-3.95 5.979-2.914-2.716 6.864z"
    ></path>
    <path
      fill="#E4761B"
      stroke="#E4761B"
      strokeWidth="0.19"
      d="M28.251 18.383l3.793 3.082-5.325.565 1.532-3.647z"
    ></path>
    <path
      fill="#E4761B"
      stroke="#E4761B"
      strokeWidth="0.19"
      d="M26.72 22.03l-1.731-7.596 3.262 3.95-1.532 3.646z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M30.968 11.52l-5.98 2.914 11.47-8.019-5.49 5.104z"
    ></path>
    <path
      fill="#C0AD9E"
      stroke="#C0AD9E"
      strokeWidth="0.19"
      d="M16.385 31.817l2.708 2.716-3.685-3.225.977.51z"
    ></path>
    <path
      fill="#CD6116"
      stroke="#CD6116"
      strokeWidth="0.19"
      d="M29.03 31.944l1.283-4.69 2.543-.35-3.826 5.04z"
    ></path>
    <path
      fill="#E2761B"
      stroke="#E2761B"
      strokeWidth="0.19"
      d="M5.404 18.75l4.687-4.738-4.083 4.172-.604.566z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M32.856 26.904l-2.542.35 1.73-5.79.812 5.44zm-7.867-12.47l-4.563-.144-4.115-5.948 8.678 6.091z"
    ></path>
    <path
      fill="#E4751F"
      stroke="#E4751F"
      strokeWidth="0.19"
      d="M32.044 21.465l-1.73 5.789-.141-2.827 1.871-2.962z"
    ></path>
    <path
      fill="#CD6116"
      stroke="#CD6116"
      strokeWidth="0.19"
      d="M26.72 22.03l5.324-.565-1.872 2.962-3.453-2.397z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M20.426 14.29l4.563.143 1.73 7.597-6.293-7.74z"
    ></path>
    <path
      fill="#E4761B"
      stroke="#E4761B"
      strokeWidth="0.19"
      d="M20.426 14.29L8.112 3.333l8.198 5.009 4.116 5.948zm-4.124 19.191l-10.7 3.185-2.136-7.843 12.836 4.658z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M6.994 20.708l4.041-3.05 3.387.757-7.428 2.293z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M14.422 18.415l-3.387-.756 1.814-7.199 1.573 7.955zM6.008 18.184l5.027-.526-4.041 3.05-.986-2.524z"
    ></path>
    <path
      fill="#CD6116"
      stroke="#CD6116"
      strokeWidth="0.19"
      d="M30.172 24.427l-2.086-1.011-1.367-1.386 3.453 2.397z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M6.008 18.185l-.356-1.808 5.383 1.282-5.027.526z"
    ></path>
    <path
      fill="#233447"
      stroke="#233447"
      strokeWidth="0.19"
      d="M27.655 25.694l.43-2.278 2.088 1.011-2.518 1.267z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M30.314 27.254l-2.659-1.561 2.518-1.266.14 2.827z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M11.035 17.659l-5.383-1.282-.439-1.521 5.822 2.803zm1.813-7.199l-1.813 7.199-5.822-2.803 7.635-4.396zm0 0l7.578 3.83-6.004 4.125-1.574-7.955z"
    ></path>
    <path
      fill="#E4761B"
      stroke="#E4761B"
      strokeWidth="0.19"
      d="M14.422 18.415l6.004-4.125 2.674 7.86-8.678-3.735zM23.1 22.15l-8.297-.168-.381-3.567L23.1 22.15z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M6.994 20.708l7.428-2.293.381 3.567-7.809-1.274zM26.72 22.03l-3.62.12-2.674-7.86 6.294 7.74z"
    ></path>
    <path
      fill="#CD6116"
      stroke="#CD6116"
      strokeWidth="0.19"
      d="M28.086 23.416l-.431 2.277-.936-3.663 1.367 1.386z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M8.112 3.333L20.426 14.29l-7.577-3.83-4.737-7.127z"
    ></path>
    <path
      fill="#E4761B"
      stroke="#E4761B"
      strokeWidth="0.19"
      d="M3.466 28.822l10.385-.414 2.45 5.073-12.835-4.659z"
    ></path>
    <path
      fill="#CD6116"
      stroke="#CD6116"
      strokeWidth="0.19"
      d="M16.302 33.481L13.85 28.41l5.1-.24-2.65 5.312z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M27.655 25.693l2.659 1.56 1.465 3.321-4.124-4.881zm-12.852-3.71l-11.337 6.84 3.528-8.115 7.81 1.274zm-.952 6.425l-10.385.414 11.337-6.84-.952 6.426zm12.868-6.378l.68 2.373-3.28.183 2.6-2.556zm-2.6 2.556l-1.018-2.437 3.619-.12-2.6 2.557z"
    ></path>
    <path
      fill="#C0AD9E"
      stroke="#C0AD9E"
      strokeWidth="0.19"
      d="M19.093 34.533l-2.791-1.052 9.705 1.57-6.915-.518z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M6.994 20.709l-1.59-1.96.604-.564.986 2.524z"
    ></path>
    <path
      fill="#D7C1B3"
      stroke="#D7C1B3"
      strokeWidth="0.19"
      d="M27.108 33.888l-1.1 1.162-9.706-1.569 10.806.407z"
    ></path>
    <path
      fill="#E4761B"
      stroke="#E4761B"
      strokeWidth="0.19"
      d="M27.63 31.037L16.302 33.48l2.65-5.311 8.678 2.867z"
    ></path>
    <path
      fill="#D7C1B3"
      stroke="#D7C1B3"
      strokeWidth="0.19"
      d="M16.302 33.48l11.328-2.444-.521 2.85-10.807-.405z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M5.213 14.856l-.372-6.124 8.008 1.728-7.636 4.396zm.795 3.329l-1.1-1.075.744-.733.356 1.808z"
    ></path>
    <path
      fill="#CD6116"
      stroke="#CD6116"
      strokeWidth="0.19"
      d="M21.246 23.726L23.1 22.15l-.265 3.854-1.59-2.277zM23.1 22.15l-1.855 1.576-2.7 1.346 4.555-2.923z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M31.78 30.575l-.506-.359-3.619-4.523 4.124 4.882z"
    ></path>
    <path
      fill="#CD6116"
      stroke="#CD6116"
      strokeWidth="0.19"
      d="M18.546 25.072l-3.743-3.09 8.298.168-4.555 2.922z"
    ></path>
    <path
      fill="#E4751F"
      stroke="#E4751F"
      strokeWidth="0.19"
      d="M22.835 26.003l.265-3.854 1.019 2.437-1.284 1.418z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M4.543 15.46l.67-.605.44 1.521-1.11-.915z"
    ></path>
    <path
      fill="#233447"
      stroke="#233447"
      strokeWidth="0.19"
      d="M22.835 26.004l-4.29-.932 2.7-1.345 1.59 2.277z"
    ></path>
    <path
      fill="#763D16"
      stroke="#763D16"
      strokeWidth="0.19"
      d="M12.849 10.46L4.84 8.732l3.27-5.399 4.738 7.127z"
    ></path>
    <path
      fill="#C0AD9E"
      stroke="#C0AD9E"
      strokeWidth="0.19"
      d="M26.007 35.05l.455 1.194-7.37-1.712 6.915.518z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M18.952 28.17l-.406-3.098 4.29.932-3.884 2.166z"
    ></path>
    <path
      fill="#E4751F"
      stroke="#E4751F"
      strokeWidth="0.19"
      d="M14.803 21.982l3.743 3.09.406 3.098-4.149-6.188z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M24.12 24.586l3.278-.183 3.876 5.813-7.155-5.63zm-9.317-2.604l4.149 6.188-5.101.239.952-6.427z"
    ></path>
    <path
      fill="#E4751F"
      stroke="#E4751F"
      strokeWidth="0.19"
      d="M24.119 24.586l4.074 5.964-5.358-4.547 1.284-1.417z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M22.835 26.004l5.358 4.547-.563.486-4.795-5.033z"
    ></path>
    <path
      fill="#F6851B"
      stroke="#F6851B"
      strokeWidth="0.19"
      d="M27.63 31.036l-8.678-2.867 3.883-2.166 4.795 5.033zm3.644-.82l-3.08.334-4.075-5.964 7.155 5.63z"
    ></path>
    <path
      fill="#C0AD9E"
      stroke="#C0AD9E"
      strokeWidth="0.19"
      d="M32.086 33.02l-1.325 2.556-4.298.669 5.623-3.225zm-5.623 3.225l-.456-1.195 1.102-1.162-.646 2.357z"
    ></path>
    <path
      fill="#C0AD9E"
      stroke="#C0AD9E"
      strokeWidth="0.19"
      d="M27.109 33.888l.811-.311-1.457 2.668.646-2.357zm-.646 2.357l1.457-2.668 4.166-.557-5.623 3.225z"
    ></path>
    <path
      fill="#161616"
      stroke="#161616"
      strokeWidth="0.19"
      d="M31.274 30.216l1.019.597-3.097.358 2.078-.955zM29.195 31.171l-1.002-.62 3.08-.335-2.078.955zm-.356.47l3.768-.438-.522 1.816-3.246-1.378z"
    ></path>
    <path
      fill="#161616"
      stroke="#161616"
      strokeWidth="0.19"
      d="M32.086 33.02l-4.166.557.92-1.935 3.246 1.377zm-4.166.557l-.811.31.522-2.85.29 2.54zm-.29-2.54l.564-.486 1.002.62-1.565-.134zm4.663-.223l.315.39-3.768.438 3.453-.828z"
    ></path>
    <path
      fill="#161616"
      stroke="#161616"
      strokeWidth="0.19"
      d="M28.84 31.642l.355-.47 3.098-.358-3.454.828zm-1.21-.606l1.21.606-.92 1.935-.29-2.54z"
    ></path>
    <path
      fill="#161616"
      stroke="#161616"
      strokeWidth="0.19"
      d="M29.195 31.172l-.356.47-1.209-.606 1.566.136z"
    ></path>
  </svg>
);

export default MetamaskIcon;
