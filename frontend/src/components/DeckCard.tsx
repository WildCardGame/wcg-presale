import { useState } from "react";
import Spinner from './Spinner';
import { simpleAction, connectWallet, spinnerShow, buyToken, addToken, getConnectInfo } from "../store/actions/wallet"
import { connect, useDispatch } from "react-redux";
import toast, { Toaster } from 'react-hot-toast';

import Moralis from "moralis";
import { useMoralis, useWeb3ExecuteFunction } from "react-moralis"
import swapABI from '../ABI/test/swapABI.json';
import usdtABI from '../ABI/test/usdtABI.json';
import { SWAP_ADDRESS, USDT_ADDRESS } from "../store/actions/wallet"
import { isMobile } from "react-device-detect";
import { MORALIS_APP_ID, MORALIS_SERVER_URL, moralisChain } from "../constants";

interface DeckCardProps {
  deckId: number;
  simple?: any
  connectWallet?: any
  buyToken?: any
};

const DeckCard = ({ deckId, simple}: DeckCardProps) => {
  const dispatch = useDispatch();
  const [buyAmount, setBuyAmount] = useState(0);

  const { account } = useMoralis()
  const { fetch, } = useWeb3ExecuteFunction();

  Moralis.start({ serverUrl: MORALIS_SERVER_URL, appId: MORALIS_APP_ID });

  const getBuyState = (res: any) => {
    Moralis.Web3API.native.getTransaction({ chain: moralisChain, transaction_hash: res.hash })
      .then((swapResult) => {
        if(swapResult === undefined){ // non transaction
          
          setTimeout(() => {
            getBuyState(res);
          }, 1000)

        } else if(swapResult.receipt_status === '1' ) { // success
          
          toast.success("Transaction success.")
          dispatch(getConnectInfo(simple.web3, simple.account, simple.account, true))
          dispatch(spinnerShow())

        }else{ // transaction failed

          dispatch(spinnerShow())
          toast.error("Transaction failed.")

        }
      })
  }

  const getApproveState = (res: any) => {
    Moralis.Web3API.native.getTransaction({ chain: moralisChain, transaction_hash: res.hash})
      .then((usdtResult) => {
        
        if(usdtResult === undefined){ // non transaction in database
          
          setTimeout(() => {
            getApproveState(res)
          }, 1000);

        } else if(usdtResult.receipt_status === "1") {// success

          dispatch(spinnerShow(true, "Please confirm transaction in your wallet."))
          fetch({ 
            onError: ()=>{
              toast.error("Transaction error.")
              dispatch(spinnerShow())
            },
            onSuccess: async (res: any)=>{
              dispatch(spinnerShow(true, "Processing Transaction..."))
              getBuyState(res)
            },
            params: {
            abi: swapABI,
            contractAddress: SWAP_ADDRESS,
            functionName: "buyForBillionaires",
            params: {
              buyer: account,
              amountUSDT: Moralis.Units.Token(buyAmount, 18)
            }
          }})

        } else { //fail

          toast.error("Transaction failed.")
          dispatch(spinnerShow())

        }
      })
  }


  const buyTokenMobile = async () => {
      dispatch(spinnerShow(true, "Processing Approving..."))
      // toast.success("Processing Approving...")
      await fetch({ 
        onError: () =>{
          toast.error("Approve error.")
          dispatch(spinnerShow())
        },
        onSuccess: async (res:any)=>{
            getApproveState(res)
        },
        params: {
        abi: usdtABI,
        contractAddress: USDT_ADDRESS,
        functionName: "approve",
        params: {
          spender: SWAP_ADDRESS,
          amount: Moralis.Units.Token(buyAmount, 18)
        }
      }})
  }


  return (
    <>
      <div><Toaster toastOptions={{className : 'm-toaster', duration : 3000, style : { fontSize: '12px' }}}/></div>
      <div
        className="rounded-lg flex flex-col h-auto  bg-center"
        style={{
          backgroundImage: `url(${simple.imagePath[deckId]})`,
          backgroundSize: "100%, 100%",
          backgroundRepeat: "no-repeat"
        }}
      >
        {/* <img src={simple.imagePath[deckId]} className="w-full" alt="" /> */}
          <div className="pb-5">
            <input type="number" id="buyAmount" value={buyAmount} className="mb-4 mt-3/4 text-white shadow appearance-none border rounded w-7/10 py-2 px-3 leading-tight focus:outline-none focus:shadow-outline bg-[#000000c0] text-[#4dff55]" onChange={(e:any)=>{
              setBuyAmount(e.target.value)
            }}/>
            <button
              className="w-8/10 rounded skew-transform font-bold bg-[#f9069c] shadow-md py-2 px-4 text-gray-50 hover:(text-white bg-pink-600) active:(bg-pink-800)"
              onClick={() => {
                  if(!simple.connectState) {
                    toast.error("Please connect Wallet first!");
                    return;
                  }

                  if(!simple.presaleStatus) {
                    toast.error("Presale is not started yet!");
                    return;
                  }
                  
                  if(Number(buyAmount) === 0 || Number(buyAmount) > simple.usdtBalance) {
                    toast.error("Insufficient value!");
                    return;
                  }

                  // if(simple.balanceCond[deckId] === false) {
                    // if(deckId === 0) {
                    //   toast.error("Only for WhiteList!");
                    //   return;
                    // }
                  // }

                  if(isMobile){
                      buyTokenMobile();
                  }else{
                    dispatch(buyToken(deckId, buyAmount, simple));
                  }
                }
              }
            >
              <p>Buy</p>
            </button>
          </div>
      </div>

      <Spinner spinnerShow={simple.show} message={simple.message} />
    </>
  );
};

const mapStateToProps = (state: any) =>{
  return { ...state }
}

const mapDispatchToProps = () => {
  return {
    simpleAction: simpleAction,
    connectWallet: connectWallet,
    spinnerShow: spinnerShow,
    buyToken: buyToken,
    addToken: addToken,
    getConnectInfo: getConnectInfo,
  }
}

 export default connect(mapStateToProps, mapDispatchToProps)(DeckCard);
