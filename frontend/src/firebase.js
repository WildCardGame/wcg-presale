import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyCjtyBE-7-hbiP9mbWnJlfRmT2cj7e7Jl4',
  authDomain: 'wildcardp2e-d70fe.firebaseapp.com',
  databaseURL: 'https://wildcardp2e-d70fe-default-rtdb.firebaseio.com',
  projectId: 'wildcardp2e-d70fe',
  storageBucket: 'wildcardp2e-d70fe.appspot.com',
  messagingSenderId: '430891799641',
  appId: '1:430891799641:web:c7324d2207a4abe7048d48',
  measurementId: 'G-N6R93RSB2S',
};
  
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
  
  
// var database = firebase.database();
var database = firebase.firestore();
  
export default database;