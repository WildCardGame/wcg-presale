import { Routes, Route } from 'react-router-dom';
import { MoralisProvider } from "react-moralis"
import { MORALIS_APP_ID, MORALIS_SERVER_URL } from './constants';
import './App.css';
import Home from './pages/Home';

function App() {
  return (
    <MoralisProvider appId={MORALIS_APP_ID} serverUrl={MORALIS_SERVER_URL}>
      <div className="App">
        <Routes>
          <Route path="*" element={<Home />} />
        </Routes>
      </div>
    </MoralisProvider>
  );
}

export default App;
