import DeckCard from '../components/DeckCard';
import { useEffect } from 'react';
import Moralis from "moralis";
import toast, { Toaster } from 'react-hot-toast';
import { useState } from "react";
import YouTube from 'react-youtube';
import { isMobile } from "react-device-detect";
import { useMoralis, } from "react-moralis";
import { useWeb3ExecuteFunction } from "react-moralis";
import { connect, useDispatch } from "react-redux";

import '../css/styles.css';
import { simpleAction, connectWallet, spinnerShow, disconnectWallet, buyToken, addToken, unlockWCT, getConnectInfo, onConnectTrustWallet } from "../store/actions/wallet";
import { MORALIS_APP_ID, MORALIS_SERVER_URL, moralisChain } from '../constants';
import database from '../firebase';
import { CHAINID, SWAP_ADDRESS } from '../store/actions/wallet';
import swapABI from "../ABI/test/swapABI.json"


const Home = (simple: any) => {
  const dispatch = useDispatch();
  const { authenticate, account,} = useMoralis();
  const { fetch,  } = useWeb3ExecuteFunction();

  const opts = {
    height: '390',
    width: '640',
    // playerVars: {
      // https://developers.google.com/youtube/player_parameters
      // autoplay: "1",
    // },
  };

  function _onReady(event: any) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  }

  const [subscribeEmail, setSubScribeEmail] = useState('');

  let _intervalHandle: any = null;

  function onSubscribe() {
    if(!(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(subscribeEmail))) {
      toast.error('Please input valid email!');
      return;
    }
    
    let date = new Date();
    let strDate = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate() + 
              " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + ":" + date.getMilliseconds();

    database.collection("presale").add({
        wallet : simple.account,
        email : subscribeEmail,
        date: strDate,
      }).catch(alert);

    toast.success('Subscribe email successfully!');
    return;
  }

  useEffect(()=>{
    const getInitData = async () =>{
      if(account && isMobile){
        await dispatch(onConnectTrustWallet(simple, account))
        await dispatch(getConnectInfo(simple.web3, account, account, true))
      }
    }
    getInitData()
  }, [account, dispatch, simple]);

  const mobileWalletConnection = () => {
    authenticate({
      onComplete: () => {  },  
      provider: "walletconnect", chainId: parseInt(CHAINID, 16)
    })
  }

  const mobileWalletDisconnection = async () => {
    await dispatch(simple.disconnectWallet())
  }

  const connectBtnMobileClick = () => {
    simple.connectState ? mobileWalletDisconnection() : mobileWalletConnection()
  } 
  const connectBtnDesktopClick = () => {
    simple.connectState ? dispatch(simple.disconnectWallet()) : dispatch(simple.connectWallet())
  }

  Moralis.start({ serverUrl: MORALIS_SERVER_URL, appId: MORALIS_APP_ID });

  const getUnlockWCT = (res: any) => {
      Moralis.Web3API.native.getTransaction({ chain: moralisChain, transaction_hash: res.hash})
        .then((unlockRes) => {
          if(unlockRes === undefined) {
            setTimeout(() => {
              getUnlockWCT(res);
            }, 1000);
          }
          else if(unlockRes.receipt_status === '1') {
            dispatch(spinnerShow(true, "Updating Transaction..."))
            dispatch(getConnectInfo(simple.web3, simple.account, simple.account, true))
            dispatch(spinnerShow())
            toast.success("UnlockWCT success.")
            clearInterval(_intervalHandle);
          }
          else {
            dispatch(spinnerShow())
            toast.error("Transaction failed.")
          }
        })
  }

  const unlockWCTMobile = async () => {
    dispatch(spinnerShow(true, "Processing Unlocking..."))
    fetch({ 
      onSuccess: async (res: any)=>{
        getUnlockWCT(res);
      },
      onError: (err:any)=>{
        toast.error("Unlock error.")
        dispatch(spinnerShow())
      },
      params: {
      abi: swapABI,
      contractAddress: SWAP_ADDRESS,
      functionName: "unlockWCT",
      params: {
        buyer: simple.account
      }
    }})
  }

  return (
    <div
      style={{
        backgroundImage: `url(${simple.bgPath})`,
        backgroundRepeat: "no-repeat",
      }}
    >
      <div><Toaster toastOptions={{className : 'm-toaster', duration : 3000, style : { fontSize: '12px' }}}/></div>
      <div className='container mx-auto'
        style={{
          fontFamily: 'Montserrat',
          // backgroundImage: `url(${simple.bgPath})`
        }}
      >
        {/* Navbar */}
        {/* <Navbar /> */}
        <nav className="z-40 w-full bg-opacity-60 flex items-center">
          <div className="top-6 gap-10 fixed items-center justify-start hidden md:flex">
            <a
              href="/#more-info"
              className="text-[#ffe41e] hover:text-[#8b7c10]"
            >
              <img className=" w-1/2" src={simple.logo} alt="" />
            </a>
            
          </div>

          <div className="z-50 fixed top-6 md:right-20 items-center justify-start flex py-2 px-4">
            <button
                className="font-bold ml-auto bg-[#f9069c] md:flex shadow-md py-2 px-4 text-white hover:bg-pink-600 active:bg-pink-800"
                onClick={() => { isMobile ? connectBtnMobileClick() : connectBtnDesktopClick() }}
              >
                {simple.connectBtnName}
            </button>
          </div>

          <button
            className="z-50 ml-auto w-14 h-14 flex fixed top-6 right-5 justify-center items-center text-gray-50"
            onClick={() => {
              // toggleMenu()
              window.location.href = 'https://wildcardgame.io';
            }}
          >
            {/* <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-8 w-8"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            > 
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M4 6h16M4 12h16M4 18h16"
              />
            </svg> */}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-8 w-8"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"
              />
            </svg>
          </button>
        </nav>

        {/* Connect Button */}
        {/* <div className="z-50 fixed top-6 md:right-20 items-center justify-start flex py-2 px-4">
          <button
              className="font-bold ml-auto bg-[#f9069c] md:flex shadow-md py-2 px-4 text-white hover:(text-white bg-[#239728])"
              onClick={() => { 
                simple.connectState ? dispatch(simple.disconnectWallet()) : dispatch(simple.connectWallet())
               }}
            >
              {simple.connectBtnName}
          </button>
        </div> */}

        <div className='flex'>
          <img className='m-auto w-3/5' src={simple.banner} alt="" />
        </div>

        {/* More Info */}
        <div
          id="more-info"
          className="flex flex-col mt-16 px-8 gap-8 items-center justify-center"
        >
          <h1 className="text-gray-50 text-5xl" style={{fontFamily: "BoomBox"}}>Wildcard Token Presale</h1>
          <p className="w-full md:max-w-screen-lg text-md text-white">
          Welcome to the Wildcard Token (WCT) Presale! 
            <br /> <br />
            Unfortunately Round 1 is closed and sold out ! Afraid you missed out ? Well, you still have a chance! Be quick and buy into Phase 2! The team is going to Permissionless in West Palm beach and they are doing a contest who sells the most presale! Make sure to buy your tokens before they are sold out!
            <br /><br />
            Wildcard Token (WCT) is the in-game currency for our exciting new play-to-earn, battle-style collectible card game, Wildcard. WCT can be spent and earned in a multitude of different ways– including in the game, in the marketplace, and in the real world. WCT is a single token economy with simulations that shows that price rise overtime while keeping the payout pool for the players the same! Watch our youtube video about the economy <a href="https://youtu.be/guG_GzwCLmg" target="_blank" rel="noreferrer" className='text-[#f9069c]'> here </a>
            <br /><br />
            As play-to-earn gaming makes its way into the mainstream, there is no better time to get involved in this largely untapped, skyrocketing market! Wildcard intends to set a new standard in the play-to-earn industry by delivering a top-shelf gaming experience with style, substance, and significant earning potential to boot.
            <br /><br />
            Approximately 2.5 billion WCT have been set aside for this presale. During Phase 1, 500 million WCT will be purchasable for $0.0004/WCT. An additional 500 million tokens will then be made available in Phase 2, which will sell for $0.0005/WCT. 
            <br /><br />
            This means that if you take advantage of Phase 1, you get to enjoy half off the public launch price, and if you hold out for Phase 2, you’ll still benefit from a 37.5% discount. Once we roll into Phase 3, tokens will be open to the public on PinkSale– so the early birds will truly get the Wildcard worms, in this case. Any unsold tokens thereafter may be sold privately to ensure our launch is a massive success. 
            <br /><br />
            This is your opportunity to grab Wildcard NFTs that hold value both in-game and in the marketplace. The NFT presale– along with the WCT presale– will help support and stabilize the Wildcard project. Funds raised through these presales will go directly into token liquidity (65%) and marketing and game development (35%). 
            <br /><br />
            Wildcard will have a starting circulating supply of approximately 10 billion tokens. While the fully diluted market supply will reach 20 billion, tokens can be burned. This makes it possible to maintain multiple balancing strategies while the game progresses. The tokens are locked and will unlock by 20% every 3 weeks post-launch to prevent dumping and to protect the token price. By 15 weeks out, 100% of whitelisted tokens will be unlocked and usable. 
            <br /><br />
            To learn more about the WCT presale, join the Wildcard Discord for updates and helpful information. Limited whitelist spots remain, so get in early for your best shot at scoring the best deals.
            <br /><br />
            Get battle-ready for the Wildcard launch in Q3 2022 by checking out alpha-version a gameplay video 
            <a href="https://www.youtube.com/channel/UCBhmCV19qQaFUlnHiN3Rovg" target="_blank" rel="noreferrer" className='text-[#f9069c]'> here. </a>
            Any additional questions? Feel free to get in touch with the Wildcard team at Info@wildcardgame.io
            <br /><br />
          </p>
        </div>

        {/* Youtube Video */}
        <div className='flex'>
          <div className='m-auto'>
            <YouTube videoId="p85hkbC2K0c" opts={opts} onReady={_onReady} />
          </div>
        </div>

        {/* Decks */}
        <div
          id="decks"
          className="flex flex-col px-8 pt-20 gap-4 items-center justify-center md:(flex-row gap-8) "
        >
          <DeckCard deckId={0} />
          {/* <DeckCard deckId={1} /> */}
          {/* <DeckCard deckId={2} /> */}

          <div className='email-subscribe-panel w-1/3'>
            <input type="email" value={subscribeEmail} id="form-field-email" className="w-4/5 h-12 mb-3 focus:outline-none border-white border-4 border-solid bg-black text-white rounded-lg px-3" style={{borderColor: "#FFFFFF"}} placeholder="Email" aria-required="true"
              onChange={(e:any)=>{
                setSubScribeEmail(e.target.value)
              }}
            />
            <button id='subcribe-btn'
              className="w-2/5 rounded font-bold bg-[#f9069c] shadow-md py-2 px-4 text-gray-50 hover:(text-white bg-pink-600) active:(bg-pink-800)"
              onClick={ ()=> {
                if(!simple.connectState) {
                  toast.error("Please connect Wallet first!");
                  return;
                }

                onSubscribe();
              }}>
                Subscribe
              </button>
          </div>
        </div>

        {/* Locked Balance Info */}
        <div
          id="unlock-info"
          className="h-auto flex-col mt-20 mx-auto gap-4 items-center justify-center w-2/5 md:(flex-row gap-8) text-[#ffe41e] border-[#f9069c] border-3 rounded-lg bg-center py-5 pr-10 pl-10"
          // style={{
            // position: 'relative',
            // backgroundSize: '170% 170%',
            // margin: 'auto'
          // }}
        >
          <div className='text-xs md:text-lg'>
            <div className='w-1/2 text-left text-gray-50'>
              Total : <span className="text-[#4dff55]">{simple.totalAirdrop}</span> WCT
            </div>
            <div className='w-1/2'>
            </div>
          </div>
          <div className='text-xs md:text-lg flex mt-4'>
            <div className='w-1/2 text-left text-gray-50'>
              Locked : <span className="text-[#4dff55]">{simple.lockAmount}</span> WCT
            </div>
            <div className='w-1/2 text-right text-gray-50'>
              Unlocked : <span className="text-[#4dff55]">{simple.unlockAmount}</span> WCT
            </div>
          </div>
          <div className='text-xs md:text-lg flex mt-4'>
            <div className='w-2/3 pt-4 text-left text-gray-50'>
              Unlockable: <span className='text-[#4dff55]'>{simple.unlockAvail}</span> WCT
            </div>
            <div className='w-1/3 text-right mt-3'>
              <button
                className="rounded unlock-transform font-bold bg-[#f9069c] text-white shadow-md py-2 px-4 text-gray-800 hover:bg-pink-600 w-full active:(bg-pink-800 shadow)"
                onClick={()=>{
                  if(!simple.connectState) {
                    toast.error("Please connect Wallet first!");
                    return;
                  }
                  if(!simple.unlockStatus) {
                    toast.error("Unlock is disabled!");
                    return;
                  }
                  if(Number(simple.unlockAvail) === 0) {
                    toast.error("No Unlockable tokens.");
                    return;
                  }
                  if(isMobile){
                    unlockWCTMobile()
                  }else{
                    dispatch(unlockWCT(simple));
                  }
                }}
              >
                <p className='text-sm text-white'>Unlock</p>
              </button>
            </div>
          </div>
        </div>        
      </div>
      {/* Footer */}
      <footer className="relative text-center lg:text-left mt-20 pt-10" style={{
          // backgroundImage: `url(${simple.footer})`, backgroundSize: "100%, 100%", backgroundRepeat: "no-repeat"
        }}>
        <img src={simple.footer} className="h-full" alt="footer" />
        <div className="absolute w-full inset-y-1/4 text-gray-50 text-center p-4" >
          <img className='m-auto h-full' src={simple.logo} alt="logo"/>  

          <div className="flex justify-center lg:mb-5 lg:mt-5 md:mb-3 md:mt-2 social-icons">
            <a href="https://discord.com/invite/wildcardp2e" target="_blank" rel="noreferrer" type="button" className="flex-shrink-0 w-7 h-7 rounded-full bg-gray-50  border-2 border-white text-black hover:text-gray-300 leading-normal uppercase hover:bg-gray-50 hover:bg-opacity-5 focus:outline-none focus:ring-0 transition duration-150 ease-in-out w-9 h-9 m-1">
              <svg role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className="w-5 h-full mx-auto"><title>Discord</title><path d="M20.317 4.3698a19.7913 19.7913 0 00-4.8851-1.5152.0741.0741 0 00-.0785.0371c-.211.3753-.4447.8648-.6083 1.2495-1.8447-.2762-3.68-.2762-5.4868 0-.1636-.3933-.4058-.8742-.6177-1.2495a.077.077 0 00-.0785-.037 19.7363 19.7363 0 00-4.8852 1.515.0699.0699 0 00-.0321.0277C.5334 9.0458-.319 13.5799.0992 18.0578a.0824.0824 0 00.0312.0561c2.0528 1.5076 4.0413 2.4228 5.9929 3.0294a.0777.0777 0 00.0842-.0276c.4616-.6304.8731-1.2952 1.226-1.9942a.076.076 0 00-.0416-.1057c-.6528-.2476-1.2743-.5495-1.8722-.8923a.077.077 0 01-.0076-.1277c.1258-.0943.2517-.1923.3718-.2914a.0743.0743 0 01.0776-.0105c3.9278 1.7933 8.18 1.7933 12.0614 0a.0739.0739 0 01.0785.0095c.1202.099.246.1981.3728.2924a.077.077 0 01-.0066.1276 12.2986 12.2986 0 01-1.873.8914.0766.0766 0 00-.0407.1067c.3604.698.7719 1.3628 1.225 1.9932a.076.076 0 00.0842.0286c1.961-.6067 3.9495-1.5219 6.0023-3.0294a.077.077 0 00.0313-.0552c.5004-5.177-.8382-9.6739-3.5485-13.6604a.061.061 0 00-.0312-.0286zM8.02 15.3312c-1.1825 0-2.1569-1.0857-2.1569-2.419 0-1.3332.9555-2.4189 2.157-2.4189 1.2108 0 2.1757 1.0952 2.1568 2.419 0 1.3332-.9555 2.4189-2.1569 2.4189zm7.9748 0c-1.1825 0-2.1569-1.0857-2.1569-2.419 0-1.3332.9554-2.4189 2.1569-2.4189 1.2108 0 2.1757 1.0952 2.1568 2.419 0 1.3332-.946 2.4189-2.1568 2.4189Z"/></svg>
            </a>

            <a href="https://mobile.twitter.com/wildcardp2e" target="_blank" rel="noreferrer"  type="button" className="flex-shrink-0 w-7 h-7 rounded-full bg-gray-50 border-2 border-white text-black hover:text-gray-300 leading-normal uppercase hover:bg-black hover:bg-opacity-5 focus:outline-none focus:ring-0 transition duration-150 ease-in-out w-9 h-9 m-1">
              <svg aria-hidden="true"
              focusable="false"
              data-prefix="fab"
                data-icon="twitter"
                className="w-5 h-full mx-auto"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"
              >
                <path
                  fill="currentColor"
                  d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"
                ></path>
              </svg>
            </a>

            <a href="https://www.youtube.com/channel/UCBhmCV19qQaFUlnHiN3Rovg" target="_blank" rel="noreferrer" type="button" className="flex-shrink-0 w-7 h-7 rounded-full bg-gray-50 border-2 border-white text-black hover:text-gray-300 leading-normal uppercase hover:bg-black hover:bg-opacity-5 focus:outline-none focus:ring-0 transition duration-150 ease-in-out w-9 h-9 m-1">
              <svg role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className="w-5 h-full mx-auto"><title>YouTube</title><path fill="currentColor" d="M23.498 6.186a3.016 3.016 0 0 0-2.122-2.136C19.505 3.545 12 3.545 12 3.545s-7.505 0-9.377.505A3.017 3.017 0 0 0 .502 6.186C0 8.07 0 12 0 12s0 3.93.502 5.814a3.016 3.016 0 0 0 2.122 2.136c1.871.505 9.376.505 9.376.505s7.505 0 9.377-.505a3.015 3.015 0 0 0 2.122-2.136C24 15.93 24 12 24 12s0-3.93-.502-5.814zM9.545 15.568V8.432L15.818 12l-6.273 3.568z"/></svg>
            </a>

            <a href="https://www.instagram.com/wildcardp2e/?utm_medium=copy_link" target="_blank" rel="noreferrer"  type="button" className="flex-shrink-0 w-7 h-7 rounded-full bg-gray-50 border-2 border-white text-black hover:text-gray-300 leading-normal uppercase hover:bg-black hover:bg-opacity-5 focus:outline-none focus:ring-0 transition duration-150 ease-in-out w-9 h-9 m-1">
              <svg role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className="w-5 h-full mx-auto"><title>Instagram</title><path fill="currentColor" d="M12 0C8.74 0 8.333.015 7.053.072 5.775.132 4.905.333 4.14.63c-.789.306-1.459.717-2.126 1.384S.935 3.35.63 4.14C.333 4.905.131 5.775.072 7.053.012 8.333 0 8.74 0 12s.015 3.667.072 4.947c.06 1.277.261 2.148.558 2.913.306.788.717 1.459 1.384 2.126.667.666 1.336 1.079 2.126 1.384.766.296 1.636.499 2.913.558C8.333 23.988 8.74 24 12 24s3.667-.015 4.947-.072c1.277-.06 2.148-.262 2.913-.558.788-.306 1.459-.718 2.126-1.384.666-.667 1.079-1.335 1.384-2.126.296-.765.499-1.636.558-2.913.06-1.28.072-1.687.072-4.947s-.015-3.667-.072-4.947c-.06-1.277-.262-2.149-.558-2.913-.306-.789-.718-1.459-1.384-2.126C21.319 1.347 20.651.935 19.86.63c-.765-.297-1.636-.499-2.913-.558C15.667.012 15.26 0 12 0zm0 2.16c3.203 0 3.585.016 4.85.071 1.17.055 1.805.249 2.227.415.562.217.96.477 1.382.896.419.42.679.819.896 1.381.164.422.36 1.057.413 2.227.057 1.266.07 1.646.07 4.85s-.015 3.585-.074 4.85c-.061 1.17-.256 1.805-.421 2.227-.224.562-.479.96-.899 1.382-.419.419-.824.679-1.38.896-.42.164-1.065.36-2.235.413-1.274.057-1.649.07-4.859.07-3.211 0-3.586-.015-4.859-.074-1.171-.061-1.816-.256-2.236-.421-.569-.224-.96-.479-1.379-.899-.421-.419-.69-.824-.9-1.38-.165-.42-.359-1.065-.42-2.235-.045-1.26-.061-1.649-.061-4.844 0-3.196.016-3.586.061-4.861.061-1.17.255-1.814.42-2.234.21-.57.479-.96.9-1.381.419-.419.81-.689 1.379-.898.42-.166 1.051-.361 2.221-.421 1.275-.045 1.65-.06 4.859-.06l.045.03zm0 3.678c-3.405 0-6.162 2.76-6.162 6.162 0 3.405 2.76 6.162 6.162 6.162 3.405 0 6.162-2.76 6.162-6.162 0-3.405-2.76-6.162-6.162-6.162zM12 16c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm7.846-10.405c0 .795-.646 1.44-1.44 1.44-.795 0-1.44-.646-1.44-1.44 0-.794.646-1.439 1.44-1.439.793-.001 1.44.645 1.44 1.439z"/></svg>
            </a>

            <a href="https://www.facebook.com/wildcardp2e" target="_blank" rel="noreferrer"  type="button" className="flex-shrink-0 w-7 h-7 rounded-full bg-gray-50 border-2 border-white text-black hover:text-gray-300 leading-normal uppercase hover:bg-black hover:bg-opacity-5 focus:outline-none focus:ring-0 transition duration-150 ease-in-out w-9 h-9 m-1">
            <svg role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className="w-5 h-full mx-auto"><title>Facebook</title><path fill="currentColor" d="M24 12.073c0-6.627-5.373-12-12-12s-12 5.373-12 12c0 5.99 4.388 10.954 10.125 11.854v-8.385H7.078v-3.47h3.047V9.43c0-3.007 1.792-4.669 4.533-4.669 1.312 0 2.686.235 2.686.235v2.953H15.83c-1.491 0-1.956.925-1.956 1.874v2.25h3.328l-.532 3.47h-2.796v8.385C19.612 23.027 24 18.062 24 12.073z"/></svg>
            </a>
            
            <a href="https://www.reddit.com/r/WildcardP2E/" target="_blank" rel="noreferrer"  type="button" className="flex-shrink-0 w-7 h-7 rounded-full bg-gray-50 border-2 border-white text-black hover:text-gray-300 leading-normal uppercase hover:bg-black hover:bg-opacity-5 focus:outline-none focus:ring-0 transition duration-150 ease-in-out w-9 h-9 m-1">
            <svg role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className="w-5 h-full mx-auto"><title>Reddit</title><path fill="currentColor" d="M12 0A12 12 0 0 0 0 12a12 12 0 0 0 12 12 12 12 0 0 0 12-12A12 12 0 0 0 12 0zm5.01 4.744c.688 0 1.25.561 1.25 1.249a1.25 1.25 0 0 1-2.498.056l-2.597-.547-.8 3.747c1.824.07 3.48.632 4.674 1.488.308-.309.73-.491 1.207-.491.968 0 1.754.786 1.754 1.754 0 .716-.435 1.333-1.01 1.614a3.111 3.111 0 0 1 .042.52c0 2.694-3.13 4.87-7.004 4.87-3.874 0-7.004-2.176-7.004-4.87 0-.183.015-.366.043-.534A1.748 1.748 0 0 1 4.028 12c0-.968.786-1.754 1.754-1.754.463 0 .898.196 1.207.49 1.207-.883 2.878-1.43 4.744-1.487l.885-4.182a.342.342 0 0 1 .14-.197.35.35 0 0 1 .238-.042l2.906.617a1.214 1.214 0 0 1 1.108-.701zM9.25 12C8.561 12 8 12.562 8 13.25c0 .687.561 1.248 1.25 1.248.687 0 1.248-.561 1.248-1.249 0-.688-.561-1.249-1.249-1.249zm5.5 0c-.687 0-1.248.561-1.248 1.25 0 .687.561 1.248 1.249 1.248.688 0 1.249-.561 1.249-1.249 0-.687-.562-1.249-1.25-1.249zm-5.466 3.99a.327.327 0 0 0-.231.094.33.33 0 0 0 0 .463c.842.842 2.484.913 2.961.913.477 0 2.105-.056 2.961-.913a.361.361 0 0 0 .029-.463.33.33 0 0 0-.464 0c-.547.533-1.684.73-2.512.73-.828 0-1.979-.196-2.512-.73a.326.326 0 0 0-.232-.095z"/></svg>
            </a>

            <a href="https://www.pinterest.ca/WildcardP2E/" target="_blank" rel="noreferrer"  type="button" className="flex-shrink-0 w-7 h-7 rounded-full bg-gray-50 border-2 border-white text-black hover:text-gray-300 leading-normal uppercase hover:bg-black hover:bg-opacity-5 focus:outline-none focus:ring-0 transition duration-150 ease-in-out w-9 h-9 m-1">
            <svg role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className="w-5 h-full mx-auto"><title>Pinterest</title><path fill="currentColor" d="M12.017 0C5.396 0 .029 5.367.029 11.987c0 5.079 3.158 9.417 7.618 11.162-.105-.949-.199-2.403.041-3.439.219-.937 1.406-5.957 1.406-5.957s-.359-.72-.359-1.781c0-1.663.967-2.911 2.168-2.911 1.024 0 1.518.769 1.518 1.688 0 1.029-.653 2.567-.992 3.992-.285 1.193.6 2.165 1.775 2.165 2.128 0 3.768-2.245 3.768-5.487 0-2.861-2.063-4.869-5.008-4.869-3.41 0-5.409 2.562-5.409 5.199 0 1.033.394 2.143.889 2.741.099.12.112.225.085.345-.09.375-.293 1.199-.334 1.363-.053.225-.172.271-.401.165-1.495-.69-2.433-2.878-2.433-4.646 0-3.776 2.748-7.252 7.92-7.252 4.158 0 7.392 2.967 7.392 6.923 0 4.135-2.607 7.462-6.233 7.462-1.214 0-2.354-.629-2.758-1.379l-.749 2.848c-.269 1.045-1.004 2.352-1.498 3.146 1.123.345 2.306.535 3.55.535 6.607 0 11.985-5.365 11.985-11.987C23.97 5.39 18.592.026 11.985.026L12.017 0z"/></svg>
            </a>

            <a href="https://www.tiktok.com/@wildcardp2e" target="_blank" rel="noreferrer"  type="button" className="flex-shrink-0 w-7 h-7 rounded-full bg-gray-50 border-2 border-white text-black hover:text-gray-300 leading-normal uppercase hover:bg-black hover:bg-opacity-5 focus:outline-none focus:ring-0 transition duration-150 ease-in-out w-9 h-9 m-1">
            <svg role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className="w-5 h-full mx-auto"><title>TikTok</title><path fill="currentColor" d="M12.525.02c1.31-.02 2.61-.01 3.91-.02.08 1.53.63 3.09 1.75 4.17 1.12 1.11 2.7 1.62 4.24 1.79v4.03c-1.44-.05-2.89-.35-4.2-.97-.57-.26-1.1-.59-1.62-.93-.01 2.92.01 5.84-.02 8.75-.08 1.4-.54 2.79-1.35 3.94-1.31 1.92-3.58 3.17-5.91 3.21-1.43.08-2.86-.31-4.08-1.03-2.02-1.19-3.44-3.37-3.65-5.71-.02-.5-.03-1-.01-1.49.18-1.9 1.12-3.72 2.58-4.96 1.66-1.44 3.98-2.13 6.15-1.72.02 1.48-.04 2.96-.04 4.44-.99-.32-2.15-.23-3.02.37-.63.41-1.11 1.04-1.36 1.75-.21.51-.15 1.07-.14 1.61.24 1.64 1.82 3.02 3.5 2.87 1.12-.01 2.19-.66 2.77-1.61.19-.33.4-.67.41-1.06.1-1.79.06-3.57.07-5.36.01-4.03-.01-8.05.02-12.07z"/></svg>
            </a>
          </div>
          <span className="text-gray-50 text-copyright">Copyright © 2021 - WILDCARD: THE PLAY TO EARN GAME</span>
        </div>
      </footer>
    </div>
  );
};

const mapStateToProps = (state: any) =>{
  state = state.simple;
  return { ...state }
}

const mapDispatchToProps = () => {
  return {
    simpleAction: simpleAction,
    connectWallet: connectWallet,
    disconnectWallet: disconnectWallet,
    spinnerShow: spinnerShow,
    buyToken: buyToken,
    addToken: addToken,
    getConnectInfo: getConnectInfo,
    onConnectTrustWallet: onConnectTrustWallet,
  }
}

 export default connect(mapStateToProps, mapDispatchToProps)(Home);
